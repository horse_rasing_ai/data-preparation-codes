#!/usr/bin/env python
# coding: utf-8

# ## 予想用データ取得スクレイピングプログラム

# %%


import custom_modules.myscraping as mysc
import MySQLdb
import time
from tqdm import tqdm

def executeSQLs(sqls):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
    
    # カーソルを取得する
    cur= con.cursor()
    
    # sqlを実行する
    for sql in sqls:
        cur.execute(sql)
    con.commit()
    
    # 終了処理
    cur.close()
    con.close()
    
def isExistRaceInfo(raceId):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
    
    # カーソルを取得する
    cur= con.cursor()

    # sqlを実行する
    sql = "select * from `p_t_race_info` where `id` = '{}'".format(raceId)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
    
    # 終了処理
    cur.close()
    con.close()

    if len(rows) == 0:
        return False
    else:
        return True
    
def getHorseEntries(raceIds):
    sqls = []
    entries_cols = ['waku', 'umaban', 'horse_id', 'horse_name', 'sex', 'age', 'jockey_id', 'jockey',
                    'stable', 'trainer_id', 'trainer', 'color', 'leg']
    for raceId in tqdm(raceIds):
        time.sleep(1)
        try:
            html = mysc.getRaceShutubaHtml(raceId)
            RaceInfo, RaceHorses = mysc.extractRaceShutubaFromHtml(html)
            # レース情報書き込みsql作成
            if not isExistRaceInfo(raceId):
                sql_write_race_info = "insert into p_t_race_info (`id`, `name`, `date`, `place_name`, `race_number`, `start_time`, `field`, `weather`, `baba`, `surface`, `distance`, `direction`, `grade`)" \
                                      " values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}')"\
                                      .format(
                                        RaceInfo['RaceId'], RaceInfo['RaceName'], RaceInfo['Date'], RaceInfo['Place'], RaceInfo['RaceNum'],
                                        RaceInfo['StartTime'], RaceInfo['Field'],RaceInfo['Weather'], RaceInfo['Baba'],
                                        RaceInfo['Surface'], RaceInfo['Distance'], RaceInfo['Direction'], RaceInfo['Grade']
                                      )
                sqls.append(sql_write_race_info)
            # 出走馬書き込みsql作成
            sql_write_horse_entries = "insert into p_t_horse_entries (`id`, `race_id`, `wakuban`, `umaban`, `horse_id`, `horse_name`, `sex`, `age`,"\
                                      "`jockey_id`, `jockey_name`,"\
                                      "`stable`, `trainer_id`, `trainer_name`, `color`, `leg`) values "
            if len(RaceHorses) == 0:
                raise mysc.NoResultError
            for index, record in enumerate(RaceHorses):
                # 同着の場合があるため、レコードIDは「レースID+記録の順番」としている（Rankを使うとキー重複エラーとなる場合が出る）
                sql_write_horse_entries_value = "('{}', '{}'".format(RaceInfo['RaceId']+("0"+str(index))[-2:], RaceInfo['RaceId'])
                for label in entries_cols:
                    sql_write_horse_entries_value += ", '{}'".format(record[label])
                sql_write_horse_entries_value += ")"
                sql_write_horse_entries += sql_write_horse_entries_value + ","
            sqls.append(sql_write_horse_entries[:-1]) # 末尾にカンマがついているため除去する
        except mysc.NoResultError:
            pass
        except Exception as e:
            print(e)
            raise e
    #         sql = "update t_race_ids set status = 'データ取得エラー' where id = {}".format(item[0])
    #         sqls.append(sql)
        finally:
            executeSQLs(sqls)
#             print(sqls)
            sqls = []
    

if __name__ == "__main__":
    print("処理を開始します")
    raceIds_header = [
        '2021050504', '2021090504', '2021030204'
    ]
    raceIds = []
    for header in raceIds_header:
        for i in range(1, 13):
            tmp = header + ("0" + str(i))[-2:]
            raceIds.append(tmp)
    getHorseEntries(raceIds)
    # print(raceIds)
    print("処理を終了します")






# %%
