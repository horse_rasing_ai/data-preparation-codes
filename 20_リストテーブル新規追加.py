#!/usr/bin/env python
# coding: utf-8

# # リスト新規追加処理
# レース結果を取得した後に、分析用の馬柱作成ステータスリストを新規追加するためのSQLを発行します。  
# 馬マスタに新規の馬を追加します。  
# 
# 考慮する必要があるエラー処理って何があるだろうか・・・？

# %%


import MySQLdb

def executeSQLs(sqls):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
    
    # カーソルを取得する
    cur= con.cursor()
    
    # sqlを実行する
    for sql in sqls:
        print("executing on '{}'".format(sql))
        cur.execute(sql)
        print("  -- done")
    con.commit()
    
    # 終了処理
    cur.close()
    con.close()

    print("***all sql executions were done***")


# %%
# 分析用馬柱作成ステータスリスト新規追加SQL
sql_1 = "insert into w_status_create_forms (id, race_id, horse_id, date_formatted, status) "\
        "select r.id, r.race_id, r.horse_id, i.date_formatted, '未処理' "\
        "from t_race_records r "\
        "join t_race_info i "\
        "on r.race_id = i.id "\
        "left join w_status_create_forms f "\
        "on r.id = f.id "\
        "where f.id is null"

# 馬マスタ新規追加SQL
sql_2 = "insert into m_horses (id, name, sex) "\
        "select distinct r.horse_id, r.horse_name, r.sex "\
        "from t_race_records r "\
        "left join m_horses h "\
        "on r.horse_id = h.id "\
        "where r.horse_id != 000 and h.id is null"

# ジョッキーマスタ新規追加SQL
sql_3 = "insert into m_jockeys (id) "\
        "select distinct r.jockey_id "\
        "from t_race_records r "\
        "left join m_jockeys j "\
        "on r.jockey_id = j.id "\
        "where j.id is null"

# トレイナーマスタ新規追加SQL
sql_4 = "insert into m_trainers (id, name) "\
        "select distinct r.trainer_id, r.trainer_name "\
        "from t_race_records r "\
        "left join m_trainers t "\
        "on r.trainer_id = t.id "\
        "where t.id is null"
executeSQLs([sql_1, sql_2, sql_3, sql_4])


# %%
# 予想用馬柱作成ステータスリスト新規追加SQL
sql_1 = "insert into p_t_status_create_forms (id, race_id, horse_id, status) "\
        "select r.id, r.race_id, r.horse_id, '未処理' "\
        "from p_t_horse_entries r "\
        "left join p_t_status_create_forms f "\
        "on r.id = f.id "\
        "where f.id is null"

# p_t_horse_entriesから新規の馬、ジョッキー、トレーナーを取得する
# 馬マスタ新規追加SQL
sql_2 = "insert into m_horses (id, name, sex) "\
        "select distinct r.horse_id, r.horse_name, r.sex "\
        "from p_t_horse_entries r "\
        "left join m_horses h "\
        "on r.horse_id = h.id "\
        "where r.horse_id != 000 and h.id is null"

# ジョッキーマスタ新規追加SQL
sql_3 = "insert into m_jockeys (id) "\
        "select distinct r.jockey_id "\
        "from p_t_horse_entries r "\
        "left join m_jockeys j "\
        "on r.jockey_id = j.id "\
        "where j.id is null"

# トレイナーマスタ新規追加SQL
sql_4 = "insert into m_trainers (id, name) "\
        "select distinct r.trainer_id, r.trainer_name "\
        "from p_t_horse_entries r "\
        "left join m_trainers t "\
        "on r.trainer_id = t.id "\
        "where t.id is null"
executeSQLs([sql_1, sql_2, sql_3, sql_4])


# %%
# 血統データマスタ新規追加SQL
sqls_update_bloods = []

# 血統データ（father）マスタ新規追加SQL
sql_1 = "insert into m_blood_father (name) "\
        "select distinct h.blood_father "\
        "from m_horses h "\
        "left join m_blood_father bf "\
        "on h.blood_father = bf.name "\
        "where bf.name is null and h.blood_father is not null"

# 血統データ（mother）マスタ新規追加SQL
sql_2 = "insert into m_blood_mother (name) "\
        "select distinct h.blood_mother "\
        "from m_horses h "\
        "left join m_blood_mother bm "\
        "on h.blood_mother = bm.name "\
        "where bm.name is null and h.blood_mother is not null"

# 血統データ（mother_mother）マスタ新規追加SQL
sql_3 = "insert into m_blood_mother_mother (name) "\
        "select distinct h.blood_mother_mother "\
        "from m_horses h "\
        "left join m_blood_mother_mother bmm "\
        "on h.blood_mother_mother = bmm.name "\
        "where bmm.name is null and h.blood_mother_mother is not null"

# 血統データ（father_father）マスタ新規追加SQL
sql_4 = "insert into m_blood_father_father (name) "\
        "select distinct h.blood_father_father "\
        "from m_horses h "\
        "left join m_blood_father_father bff "\
        "on h.blood_father_father = bff.name "\
        "where bff.name is null and h.blood_father_father is not null"

# 血統データ（mother_father）マスタ新規追加SQL
sql_5 = "insert into m_blood_mother_father (name) "\
        "select distinct h.blood_mother_father "\
        "from m_horses h "\
        "left join m_blood_mother_father bmf "\
        "on h.blood_mother_father = bmf.name "\
        "where bmf.name is null and h.blood_mother_father is not null"

# 血統データ（father_mother）マスタ新規追加SQL
sql_6 = "insert into m_blood_father_mother (name) "\
        "select distinct h.blood_father_mother "\
        "from m_horses h "\
        "left join m_blood_father_mother bfm "\
        "on h.blood_father_mother = bfm.name "\
        "where bfm.name is null and h.blood_father_mother is not null"

executeSQLs([sql_1, sql_2, sql_3, sql_4, sql_5, sql_6])

# %%
# レース出走リスト更新
# t_race_entriesを一度削除
sql_1 = "drop table t_race_entries"

# 再作成
sql_2 = "create table t_race_entries "\
        "select `t_race_records`.`race_id` AS `race_id`,max(`t_race_records`.`umaban`) AS `entries` "\
        "from `t_race_records` group by `t_race_records`.`race_id`"

executeSQLs([sql_1, sql_2])

# %%
