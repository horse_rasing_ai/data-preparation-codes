#!/usr/bin/env python
# coding: utf-8

# # 馬・ジョッキーデータ更新
# 【ToDo】updateHorseInfoとupdateJockeyInfoはちゃんとしたところに移動する

# %%
import custom_modules.myscraping as mysc
import MySQLdb
import time
from tqdm import tqdm
from bs4 import BeautifulSoup
    
def fetch_horseids(limit):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur = con.cursor()

    # クエリを実行する
    sql = "select `id` from `m_horses` where `birthday` is null limit {}".format(limit)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows

def fetch_jockeyids(limit):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur = con.cursor()

    # クエリを実行する
    sql = "select `id` from `m_jockeys` where `name` is null limit {}".format(limit)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows

def executeSQLs(sqls):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
    
    # カーソルを取得する
    cur= con.cursor()
    
    # sqlを実行する
    for sql in sqls:
        cur.execute(sql)
    con.commit()
    
    # 終了処理
    cur.close()
    con.close()

def updateHorseInfo(limit):
    idlist = fetch_horseids(limit)
    sqls = []
    count = 1
    for item in tqdm(idlist):
        count += 1
        time.sleep(1)
        try:
            html = mysc.getHorseInfoHtml(item[0])
            HorseInfo = mysc.extractHorseInfoFromHtml(html)
            sql_update_horse_info = "update `m_horses` set `sex`='{}',`birthday`='{}',`color`='{}', `farm`='{}',`total_wins`='{}', `blood_father`='{}', `blood_mother`='{}', `blood_father_father`='{}', `blood_father_mother`='{}', `blood_mother_father`='{}', `blood_mother_mother`='{}', `is_active`={} where `id`='{}'"\
                .format(HorseInfo['sex'], HorseInfo['birthday'], HorseInfo['color'], HorseInfo['farm'], HorseInfo['total_wins'],HorseInfo['blood_father'], HorseInfo['blood_mother'], HorseInfo['blood_father_father'],HorseInfo['blood_father_mother'], HorseInfo['blood_mother_father'], HorseInfo['blood_mother_mother'], HorseInfo['is_active'], HorseInfo['id'])
            sqls.append(sql_update_horse_info)
        except mysc.NoResultError as e:
            # print(e)
            pass
        except Exception as e:
            # print(e)
            pass
        finally:
#             print(sqls)
            executeSQLs(sqls)
            sqls = []

def updateJockeyInfo(limit):
    idlist = fetch_jockeyids(limit)
    sqls = []
    count = 1
    for item in tqdm(idlist):
        count += 1
        time.sleep(1)
        try:
            html = mysc.getJockeyInfoHtml(item[0])
            JockeyInfo = mysc.extractJockeyInfoFromHtml(html)
            if 'home_town' in JockeyInfo:
                sql_update_jockey_info = "update `m_jockeys` set `name`='{}', `home_town`='{}', `height`='{}', `weight`='{}', `blood_type`='{}' where `id` = '{}'"                                            .format(JockeyInfo['name'], JockeyInfo['home_town'], JockeyInfo['height'], JockeyInfo['weight'], JockeyInfo['blood_type'], JockeyInfo['id'])
            else:
                sql_update_jockey_info = "update `m_jockeys` set `name`='{}' where id='{}'".format(JockeyInfo['name'], JockeyInfo['id'])
            sqls.append(sql_update_jockey_info)
        except mysc.NoResultError as e:
            # print(item[0], e)
            pass
        except Exception as e:
            # print(item[0], e)
            pass
        finally:
            executeSQLs(sqls)
#             print(sqls)
            sqls = []

updateHorseInfo(1000)
updateJockeyInfo(1000)


# %%
