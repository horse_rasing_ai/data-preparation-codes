#!/usr/bin/env python
# coding: utf-8

# ## 予想用馬柱データ作成

# In[4]:

import MySQLdb
from tqdm import tqdm
import pandas as pd
import numpy as np
    
def fetch_record_ids(condition, limit):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur= con.cursor()

    # クエリを実行する
    sql = "select * from `p_t_status_create_forms` where `status` = '{}' order by `id` desc limit {}".format(condition, limit)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows

def fetch_past_record_by_horse_id(horse_id, race_id):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur= con.cursor()

    # クエリを実行する
    sql = "select tr.*, tn.entries as 'entries', ti.grade as 'grade', ti.surface as 'surface', ti.distance as 'distance', ti.direction as 'direction' from `t_race_records` as tr, `t_race_info` as ti, `t_race_entries` as tn where tr.`race_id` = ti.`id` and tr.`race_id` = tn.`race_id` and tr.`horse_id` = '{}' order by `id` desc".format(horse_id)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows

def executeSQLs(sqls):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
    
    # カーソルを取得する
    cur= con.cursor()
    
    # sqlを実行する
    for sql in sqls:
        cur.execute(sql)
    con.commit()
    
    # 終了処理
    cur.close()
    con.close()

def createForms(condition, limit):
    columns = ["id","race_id","rank","wakuban","umaban","horse_id","horse_name","sex","age","weight","weight_change","jockey_id","jockey_name","jockey_weight","time_all","time_3f","passage_rate","ninki","odds","stable","trainer_id","trainer_name","created","updated","is_deleted","entries","grade","surface","distance","direction"]
    # 処理内容：レースレコードを取得して、それぞれの出走馬の過去のレース情報を取得し、テーブルに登録する
    # レースレコードごとに過去のレースデータを取得するSQLを取得する
    idlist = fetch_record_ids(condition, limit)
    sqls = []
    
    for item in tqdm(idlist):
        form = {}
        form['id'] = item[0]
        race_id = item[1]
        form['horse_id'] = item[2]
        # 取得したデータを整形して、SQL文を作る
        # 過去の戦績を取得するSQLを作成する(param: horse_id, race_id, )
        past_records = fetch_past_record_by_horse_id(form['horse_id'], race_id)
        df = pd.DataFrame(past_records, columns=columns)
        form['runs'] = len(past_records)
        form['wins'] = (df['rank']==1).sum()
        form['shows'] = (df['rank']<=3).sum()
        form['placed'] = (df['rank']<=5).sum()
        if form['runs'] > 0:
            form['rate_wins'] = form['wins']/form['runs']
            form['rate_shows'] = form['shows']/form['runs']
            form['rate_placed'] = form['placed']/form['runs']
        else:
            form['rate_wins'] = None
            form['rate_shows'] = None
            form['rate_placed'] = None
            
        if form['runs'] > 0:
            for i in range(5):
                if i < form['runs']:
                    j = i
                else:
                    j = form['runs'] - 1
                form["race_id_pre_{}".format(i+1)] = df['race_id'][j]
                form["race_grade_pre_{}".format(i+1)] = df['grade'][j]
                form["entries_pre_{}".format(i+1)] = df['entries'][j]
                form["surface_pre_{}".format(i+1)] = df['surface'][j]
                form["distance_pre_{}".format(i+1)] = df['distance'][j]
                form["direction_pre_{}".format(i+1)] = df['direction'][j]
                form["sex_pre_{}".format(i+1)] = df['sex'][j]
                form["age_pre_{}".format(i+1)] = df['age'][j]
                form["rank_pre_{}".format(i+1)] = df['rank'][j]
                form["time_all_pre_{}".format(i+1)] = str(df['time_all'][j]).replace('0 days ','')
                form["time_3f_pre_{}".format(i+1)] = str(df['time_3f'][j]).replace('0 days ','')
                form["weight_pre_{}".format(i+1)] = df['weight'][j]
                form["weight_change_pre_{}".format(i+1)] = df['weight_change'][j]
                form["racing_weight_pre_{}".format(i+1)] = df['jockey_weight'][j]
                form["horse_number_pre_{}".format(i+1)] = df['umaban'][j]
                form["bracket_number_pre_{}".format(i+1)] = df['wakuban'][j]
                form["jockey_id_pre_{}".format(i+1)] = df['jockey_id'][j]
                form["trainer_id_pre_{}".format(i+1)] = df['trainer_id'][j]
                form["odds_pre_{}".format(i+1)] = df['odds'][j]
                form["ninki_pre_{}".format(i+1)] = '0' if df['ninki'][j] is np.nan else df['ninki'][j]
        
        sql_insert = "insert into p_t_forms (`id`,`horse_id`,`runs`,`wins`,`shows`,`placed`,`rate_wins`,`rate_show`,`rate_placed`"
        
        if form['runs'] > 0:
            for i in range(5):
                sql_insert += ",`race_id_pre_{0}`,`race_grade_pre_{0}`,`entries_pre_{0}`,`surface_pre_{0}`,`distance_pre_{0}`,`direction_pre_{0}`,`sex_pre_{0}`,`age_pre_{0}`,\
                                `rank_pre_{0}`,`time_all_pre_{0}`,`time_3f_pre_{0}`,`weight_pre_{0}`,`weight_change_pre_{0}`,`racing_weight_pre_{0}`,`horse_number_pre_{0}`,\
                                `bracket_number_pre_{0}`,`jockey_id_pre_{0}`,`trainer_id_pre_{0}`,`odds_pre_{0}`,`ninki_pre_{0}`".format(i+1)                       

        sql_insert += ") values ("
                       
        for k in form:
            if form[k] is None:
                sql_insert += "null,"
            else:
                sql_insert += "'{}',".format(form[k])
        sql_insert = sql_insert[:-1] + ")"
        sqls.append(sql_insert)
        sql_update_status = "update `p_t_status_create_forms` set `status` = 'データ取得完了' where id ='{}'".format(form['id'])
        sqls.append(sql_update_status)
        # print(sqls)
        # SQLを実行する
        executeSQLs(sqls)
        sqls = []

def updateAddPreviousOdds(condition, limit):
    columns = ["id","race_id","rank","wakuban","umaban","horse_id","horse_name","sex","age","weight","weight_change","jockey_id","jockey_name","jockey_weight","time_all","time_3f","passage_rate","ninki","odds","stable","trainer_id","trainer_name","created","updated","is_deleted","entries","grade","surface","distance","direction"]
    # 処理内容：馬柱にオッズを追加する
    idlist = fetch_record_ids(condition, limit)
    sqls = []
    # sqlを実行する間隔を制御するための回数
    cnt_sql = 0
    
    for item in tqdm(idlist):
        cnt_sql += 1
        form = {}
        form['id'] = item[0]
        race_id = item[1]
        form['horse_id'] = item[2]
        # 取得したデータを整形して、SQL文を作る
        # 過去の戦績を取得するSQLを作成する(param: horse_id, race_id, )
        past_records = fetch_past_record_by_horse_id(form['horse_id'], race_id)
        df = pd.DataFrame(past_records, columns=columns)
        form['runs'] = len(past_records)
        
        if len(past_records) == 0:
            sql_update_status = "update `w_status_create_forms` set `status` = 'データ更新完了' where id ='{}'".format(form['id'])
            executeSQLs([sql_update_status])
            continue

        for i in range(min(5, form['runs'])):
            form["odds_pre_{}".format(i+1)] = df['odds'][i]
            form["ninki_pre_{}".format(i+1)] = df['ninki'][i] if df['ninki'][i] is np.nan else '0' 

        sql_update = "update `t_forms` set "

        for i in range(min(5, form['runs'])):
            sql_update += "`odds_pre_{0}` = '{1}',`ninki_pre_{0}` = '{2}',".format(i+1, form["odds_pre_"+str(i+1)], form["ninki_pre_"+str(i+1)])

        sql_update = sql_update[:-1] + " where id = '{}'".format(form['id'])
        sqls.append(sql_update)
        sql_update_status = "update `w_status_create_forms` set `status` = 'データ更新完了' where id ='{}'".format(form['id'])
        sqls.append(sql_update_status)
        # print(sqls)
        if cnt_sql > 10:
            # SQLを実行する
            executeSQLs(sqls)
            sqls = []
            cnt_sql = 0

if __name__ == "__main__":
    condition = '未処理'
    # condition = '処理エラー'
    print("処理を開始します")
    for i in range(1):
        createForms(condition, 1000)
#         updateAddPreviousOdds(condition, 10000)
    print("処理を終了します")


# In[ ]: