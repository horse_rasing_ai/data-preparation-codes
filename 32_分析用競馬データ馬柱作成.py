#!/usr/bin/env python
# coding: utf-8

# ## 分析用馬柱作成用プログラム
# ToDo: w_status_create_formsにdate_formattedを追加して正しい馬柱が作られるようにする
# ToDo: 正しい馬柱を作り直す

# %%
import custom_modules.myscraping as mysc
import MySQLdb
from tqdm import tqdm
import pandas as pd
import numpy as np
    
def fetch_record_ids(condition, limit):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur= con.cursor()

    # クエリを実行する
    sql = "select * from `w_status_create_forms` where `status` = '{}' order by `date_formatted` desc limit {}".format(condition, limit)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows

def fetch_past_record_by_horse_id(horse_id, date_formatted):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur= con.cursor()

    # クエリを実行する
    sql = "select tr.*, tn.entries as 'entries', ti.grade as 'grade', ti.surface as 'surface', ti.distance as 'distance', ti.direction as 'direction'"\
          " from `t_race_records` as tr, `t_race_info` as ti, `t_race_entries` as tn"\
          " where tr.`race_id` = ti.`id` and tr.`race_id` = tn.`race_id` and tr.`horse_id` = '{}' and ti.`date_formatted` < '{}' order by ti.`date_formatted` desc".format(horse_id, date_formatted)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows

def fetch_race_info(limit, offset):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur= con.cursor()

    # クエリを実行する
    sql = "select id, date_formatted from `t_race_info` limit {} offset {}".format(limit, offset)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows

def executeSQLs(sqls):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
    
    # カーソルを取得する
    cur= con.cursor()
    
    # sqlを実行する
    for sql in sqls:
        cur.execute(sql)
    con.commit()
    
    # 終了処理
    cur.close()
    con.close()

def createForms(condition, limit):
    columns = ["id","race_id","rank","wakuban","umaban","horse_id","horse_name","sex","age","weight","weight_change","jockey_id","jockey_name","jockey_weight","time_all","time_3f","passage_rate","ninki","odds","stable","trainer_id","trainer_name","created","updated","is_deleted","entries","grade","surface","distance","direction"]
    # 処理内容：レースレコードを取得して、それぞれの出走馬の過去のレース情報を取得し、テーブルに登録する
    # レースレコードごとに過去のレースデータを取得するSQLを取得する
    idlist = fetch_record_ids(condition, limit)
    sqls = []
    
    for item in tqdm(idlist):
        form = {}
        form['id'] = item[0]
        form['horse_id'] = item[2]
        date_formatted = item[3]
        # 取得したデータを整形して、SQL文を作る
        # 過去の戦績を取得するSQLを作成する(param: horse_id, date_formatted)
        past_records = fetch_past_record_by_horse_id(form['horse_id'], date_formatted)
        df = pd.DataFrame(past_records, columns=columns)
        form['runs'] = len(past_records)
        form['wins'] = (df['rank']==1).sum()
        form['shows'] = (df['rank']<=3).sum()
        form['placed'] = (df['rank']<=5).sum()
        if form['runs'] > 0:
            form['rate_wins'] = form['wins']/form['runs']
            form['rate_shows'] = form['shows']/form['runs']
            form['rate_placed'] = form['placed']/form['runs']
        else:
            form['rate_wins'] = None
            form['rate_shows'] = None
            form['rate_placed'] = None
            
        for i in range(min(5, form['runs'])):
            form["race_id_pre_{}".format(i+1)] = df['race_id'][i]
            form["race_grade_pre_{}".format(i+1)] = df['grade'][i]
            form["entries_pre_{}".format(i+1)] = df['entries'][i]
            form["surface_pre_{}".format(i+1)] = df['surface'][i]
            form["distance_pre_{}".format(i+1)] = df['distance'][i]
            form["direction_pre_{}".format(i+1)] = df['direction'][i]
            form["sex_pre_{}".format(i+1)] = df['sex'][i]
            form["age_pre_{}".format(i+1)] = df['age'][i]
            form["rank_pre_{}".format(i+1)] = df['rank'][i]
            form["time_all_pre_{}".format(i+1)] = str(df['time_all'][i]).replace('0 days ','')
            form["time_3f_pre_{}".format(i+1)] = str(df['time_3f'][i]).replace('0 days ','')
            form["weight_pre_{}".format(i+1)] = df['weight'][i]
            form["weight_change_pre_{}".format(i+1)] = df['weight_change'][i]
            form["racing_weight_pre_{}".format(i+1)] = df['jockey_weight'][i]
            form["horse_number_pre_{}".format(i+1)] = df['umaban'][i]
            form["bracket_number_pre_{}".format(i+1)] = df['wakuban'][i]
            form["jockey_id_pre_{}".format(i+1)] = df['jockey_id'][i]
            form["trainer_id_pre_{}".format(i+1)] = df['trainer_id'][i]
            form["odds_pre_{}".format(i+1)] = df['odds'][i]
            form["ninki_pre_{}".format(i+1)] = '0' if df['ninki'][i] is np.nan else df['ninki'][i]
        
        sql_insert = "insert into t_forms (`id`,`horse_id`,`runs`,`wins`,`shows`,`placed`,`rate_wins`,`rate_show`,`rate_placed`"
        
        for i in range(min(5, form['runs'])):
            sql_insert += ",`race_id_pre_{0}`,`race_grade_pre_{0}`,`entries_pre_{0}`,`surface_pre_{0}`,`distance_pre_{0}`,`direction_pre_{0}`,`sex_pre_{0}`,`age_pre_{0}`, `rank_pre_{0}`,`time_all_pre_{0}`,`time_3f_pre_{0}`,`weight_pre_{0}`,`weight_change_pre_{0}`,`racing_weight_pre_{0}`,`horse_number_pre_{0}`, `bracket_number_pre_{0}`,`jockey_id_pre_{0}`,`trainer_id_pre_{0}`,`odds_pre_{0}`,`ninki_pre_{0}`".format(i+1)                       

        sql_insert += ") values ("
                       
        for k in form:
            if form[k] is None:
                sql_insert += "null,"
            else:
                sql_insert += "'{}',".format(form[k])
        sql_insert = sql_insert[:-1] + ")"
        sqls.append(sql_insert)
        sql_update_status = "update `w_status_create_forms` set `status` = 'データ取得完了' where id ='{}'".format(form['id'])
        sqls.append(sql_update_status)
        # print(sqls)
        # SQLを実行する
        try:
            executeSQLs(sqls)
            sqls = []
        except Exception as e:
            print(form['id'], e)
            break

if __name__ == "__main__":
    condition = '未処理'
    # condition = '処理エラー'
    print("処理を開始します")
    # for i in range(1):
    createForms(condition, 1000)
    print("処理を終了します")

# %%
