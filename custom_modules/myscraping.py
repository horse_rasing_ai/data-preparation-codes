# %%
import requests
from bs4 import BeautifulSoup
import re
import numpy as np

class NoResultError(Exception):
    pass

def converGradeChar(grade):
    if grade == 'GⅢ' or grade == 'g3':
        return 'G3'
    elif grade == 'GⅡ' or grade == 'g2':
        return 'G2'
    elif grade == 'GⅠ' or grade == 'g1':
        return 'G1'
    elif grade == 'J・GⅢ' or grade == 'jg3':
        return 'JG3'
    elif grade == 'J・GⅡ' or grade == 'jg2':
        return 'JG2'
    elif grade == 'J・GⅠ' or grade == 'jg1':
        return 'JG1'
    elif grade == '新設重賞' or grade == '重賞' or grade == 'new':
        return 'G'
    elif grade == 'JpnⅠ' or grade == 'jpn1':
        return 'J1'
    elif grade == 'JpnⅡ' or grade == 'jpn2':
        return 'J2'
    elif grade == 'JpnⅢ' or grade == 'jpn3':
        return 'J3'
    elif grade == '５００万下' or grade == '１勝クラス':
        return '1勝クラス'
    elif grade == '１０００万下' or grade == '２勝クラス':
        return '2勝クラス'   
    elif grade == '１６００万下' or grade == '３勝クラス':
        return '3勝クラス'
    elif grade == 'オープン':
        return 'OP'
    elif grade == '未勝利':
        return '未勝利'
    else:
        return 'error'
    
def converConditionChar(cond):
    if '障害３歳以上' in cond:
        return '3歳上障害'
    elif '障害４歳以上' in cond:
        return '4歳上障害'
    elif '３歳以上' in cond:
        return '3歳上'
    elif '４歳以上' in cond:
        return '4歳上'
    elif '２歳' in cond:
        return '2歳'
    elif '３歳' in cond:
        return '3歳'
    else:
        return 'error'

# 予想取得時Utility
def converLeg(typeChar):
    if typeChar == 'horse_race_type01':
        return '逃'
    elif typeChar == 'horse_race_type02':
        return '先行'
    elif typeChar == 'horse_race_type03':
        return '差'
    elif typeChar == 'horse_race_type04':
        return '追込'
    else:
        return 'no_data'

def getRaceResultHtml(raceId):
    url = 'https://race.netkeiba.com/race/result.html?race_id={}&rf=race_list'.format(raceId)
    html = requests.get(url)
    html.encoding = 'EUC-JP'
    return html

def getHorseInfoHtml(horseId):
    url = 'https://db.netkeiba.com/horse/{}'.format(horseId)
    html = requests.get(url)
    html.encoding = 'EUC-JP'
    return html

def getJockeyInfoHtml(jockeyId):
    url = 'https://db.netkeiba.com/jockey/profile/{}/'.format(jockeyId)
    html = requests.get(url)
    html.encoding = 'EUC-JP'
    return html

def getGradedRaceInfoHtml(year):
    url = 'https://www.jra.go.jp/datafile/seiseki/replay/{}/jyusyo.html'.format(year)
    html = requests.get(url)
    html.encoding = 'shift_jis'
    return html

def getRaceShutubaHtml(raceId):
    url = 'https://race.netkeiba.com/race/shutuba_past.html?race_id={}'.format(raceId)
    html = requests.get(url)
    html.encoding = 'EUC-JP'
    return html

def extractRaceResutFromHtml(html):
    soup = BeautifulSoup(html.text, 'html.parser', from_encoding='EUC-JP')
    # レース結果がない場合終了する
    if soup.find(attrs={'class': 'RaceName'}) is None:
        raise NoResultError()
    
    RaceInfo = {} # レース情報
    RaceResult = [] # レース結果
    # レース情報を取得する
    RaceInfo['RaceId'] = re.search(r'\d+', soup.find('meta', attrs={'property': 'og:url', 'content': True})['content']).group()
    ## レース日
    RaceInfo['Date'] = soup.select('#RaceList_DateList dd.Active')[0].text
    ## レース場
    RaceInfo['Place'] = soup.select('div.RaceData02 span')[1].text
    ## レース番号
    RaceInfo['RaceNum'] = soup.find_all(attrs={'class': 'RaceNum'})[0].text
    ## レース名
    RaceInfo['RaceName'] = soup.find_all(attrs={'class': 'RaceName'})[0].text.strip()
    ## レース時間/レース場/天候/馬場
    elems = soup.find_all(attrs={'class': 'RaceData01'})[0].text.strip().replace(' ', '').replace('\n', '').split('/')
    RaceInfo['StartTime'] = elems[0]
    RaceInfo['Field'] = elems[1]
    RaceInfo['Weather'] = elems[2].split(':')[1]
    RaceInfo['Baba'] = elems[3].split(':')[1]
    ## 距離/地面/方向（Fieldより抽出）
    tmp = re.match(r'(.)(\d{4})m\((.+)\)', elems[1])
    RaceInfo['Surface'] = tmp.group(1)
    RaceInfo['Distance'] = tmp.group(2)
    RaceInfo['Direction'] = tmp.group(3)
    ## レース種別/グレード
    gradeIcon = ''
    raceNameObj = soup.select('.RaceName > span')
    if len(raceNameObj) > 0:
        for c in raceNameObj[0]['class']:
            if c == 'Icon_GradeType1':
                gradeIcon = 'G1'
            elif c == 'Icon_GradeType2':
                gradeIcon = 'G2'
            elif c == 'Icon_GradeType3':
                gradeIcon = 'G3'
            else:
                pass
    raceInfoObj = soup.select('.RaceData02 > span')
    if gradeIcon == '':
        RaceInfo['Grade'] = converConditionChar(raceInfoObj[3].text)+converGradeChar(raceInfoObj[4].text)
    else:
        RaceInfo['Grade'] = gradeIcon
    ## オッズを取得する
    try:
        if len(soup.select('tr.Tansho > td.Payout')[0].text.split('円')) > 2:
            RaceInfo['odds_win'] = (float(soup.select('tr.Tansho > td.Payout')[0].text.split('円')[0].replace(',',''))+float(soup.select('tr.Tansho > td.Payout')[0].text.split('円')[1].replace(',','')))/2/100
        else:
            RaceInfo['odds_win'] = float(soup.select('tr.Tansho > td.Payout')[0].text[:-1].replace(',',''))/100
    except Exception:
        RaceInfo['odds_win'] = 0
    RaceInfo['odds_show_1st'] = float(soup.select('tr.Fukusho > td.Payout')[0].text.split('円')[0].replace(',',''))/100
    RaceInfo['odds_show_2nd'] = float(soup.select('tr.Fukusho > td.Payout')[0].text.split('円')[1].replace(',',''))/100
    try:
        RaceInfo['odds_show_3rd'] = float(soup.select('tr.Fukusho > td.Payout')[0].text.split('円')[2].replace(',',''))/100
    except Exception:
        RaceInfo['odds_show_3rd'] = 0

    try:
        if len(soup.select('tr.Wakuren > td.Payout')[0].text.split('円')) > 2:
            RaceInfo['odds_bracket_quinella'] = (float(soup.select('tr.Wakuren > td.Payout')[0].text.split('円')[0].replace(',',''))+float(soup.select('tr.Wakuren > td.Payout')[0].text.split('円')[1].replace(',','')))/2/100
        else:
            RaceInfo['odds_bracket_quinella'] = float(soup.select('tr.Wakuren > td.Payout')[0].text[:-1].replace(',',''))/100
    except Exception:
        RaceInfo['odds_bracket_quinella'] = 0

    try:
        if len(soup.select('tr.Umaren > td.Payout')[0].text.split('円')) > 2:
            RaceInfo['odds_quinella'] = (float(soup.select('tr.Umaren > td.Payout')[0].text.split('円')[0].replace(',',''))+float(soup.select('tr.Umaren > td.Payout')[0].text.split('円')[1].replace(',','')))/2/100
        else:
            RaceInfo['odds_quinella'] = float(soup.select('tr.Umaren > td.Payout')[0].text[:-1].replace(',',''))/100
    except Exception:
        RaceInfo['odds_quinella'] = 0

    RaceInfo['odds_quinella_place_1st_2nd'] = float(soup.select('tr.Wide > td.Payout')[0].text.split('円')[0].replace(',',''))/100
    RaceInfo['odds_quinella_place_1st_3rd'] = float(soup.select('tr.Wide > td.Payout')[0].text.split('円')[1].replace(',',''))/100
    RaceInfo['odds_quinella_place_2nd_3rd'] = float(soup.select('tr.Wide > td.Payout')[0].text.split('円')[2].replace(',',''))/100
    try:
        if len(soup.select('tr.Umatan > td.Payout')[0].text.split('円')) > 2:
            RaceInfo['odds_exacta'] = (float(soup.select('tr.Umatan > td.Payout')[0].split('円')[0].replace(',',''))+float(soup.select('tr.Umatan > td.Payout')[0].split('円')[1].replace(',','')))/2/100
        else:
            RaceInfo['odds_exacta'] = float(soup.select('tr.Umatan > td.Payout')[0].text[:-1].replace(',',''))/100
    except Exception:
        RaceInfo['odds_exacta'] = 0
    RaceInfo['odds_trio'] = float(soup.select('tr.Fuku3 > td.Payout')[0].text[:-1].replace(',',''))/100
    try:
        if len(soup.select('tr.Tan3 > td.Payout')[0].text.split('円')) > 2:
            RaceInfo['odds_trifecta'] = (float(soup.select('tr.Tan3 > td.Payout')[0].split('円')[0].replace(',',''))+float(soup.select('tr.Tan3 > td.Payout')[0].split('円')[1].replace(',','')))/2/100
        else:
            RaceInfo['odds_trifecta'] = float(soup.select('tr.Tan3 > td.Payout')[0].text[:-1].replace(',',''))/100
    except Exception:
        RaceInfo['odds_trifecta'] = 0


    # レース結果を取得する
    HorseList = soup.select('.HorseList')
    for line in HorseList:
        result = {}
        result['Rank'] = line.select('.Rank')[0].text
        if not result['Rank'].isdecimal():
            continue
        result['Waku'] = line.select('.Num')[0].div.text
        result['Umaban'] = line.select('.Num.Txt_C')[0].div.text
        result['HorseId'] = re.search(r'\d+', line.select('.Horse_Name')[0].a['href']).group()
        result['HorseName'] = line.select('.Horse_Name')[0].a.text
        SexAge = line.select('.Lgt_Txt.Txt_C')[0].text.replace('\n', '')
        result['Sex'] = SexAge[0]
        result['Age'] = SexAge[1:]
        # エラー対応 2021/2/12：体重変化の記載のないデータに対応
        # ----Before-----
        # result['Weight'] = line.select('td.Weight')[0].text.replace(')','').split('(')[0]
        # result['WeightChange'] = line.select('td.Weight')[0].text.replace(')','').split('(')[1]
        # ----After------
        temp = line.select('td.Weight')[0].text.replace(')','').split('(')
        if len(temp) > 1:
            result['Weight'] = line.select('td.Weight')[0].text.replace(')','').split('(')[0]
            result['WeightChange'] = line.select('td.Weight')[0].text.replace(')','').split('(')[1]
        else:
            result['Weight'] = temp[0]
            result['WeightChange'] = '0'
        # エラー対応終了 2021/2/12
        # エラー対応2021/2/14:ジョッキーIDがないデータに対応
        # ----Before----
        # result['JockeyId'] = re.search(r'\d+', line.select('td.Jockey')[0].a['href']).group()
        # result['JockeyName'] = line.select('td.Jockey')[0].a.text.strip().replace('▲', '').replace('☆', '')
        # ----After-----
        jockeyIdAtag = line.select('td.Jockey')[0].a
        if jockeyIdAtag is None:
            result['JockeyId'] = 'no_id'
            result['JockeyName'] = line.select('td.Jockey')[0].text.strip().replace('\n', '').replace('▲', '').replace('☆', '')
        else:
            result['JockeyId'] = re.search(r'\d+', line.select('td.Jockey')[0].a['href']).group()
            result['JockeyName'] = line.select('td.Jockey')[0].a.text.strip().replace('\n', '').replace('▲', '').replace('☆', '')
        # エラー対応終了 2021/2/14
        result['JockeyWeight'] = line.select('span.JockeyWeight')[0].text
        # mysqlのTime型に挿入することを想定して00:(hh:)を追加している
        result['Time'] = "00:" + line.select('td.Time span.RaceTime')[0].text
        result['Time3F'] = line.select('td.Time')[2].text.replace('\n', '')
        result['PassageRate'] = line.select('td.PassageRate')[0].text.replace('\n', '')
        # オッズ取得修正 2021/2/14:オッズが人気順と間違えて取得していたため修正
        # ----Before----
        # result['Odds'] = line.select('td.Odds')[0].span.text
        # ----After-----
        ninki = line.select('td.Odds')[0].span.text
        if ninki == '':
            result['Ninki'] = '0'
            result['Odds'] = '0'
        else:
            result['Ninki'] = line.select('td.Odds')[0].span.text
            result['Odds'] = line.select('td.Odds')[1].span.text
        # オッズ修正終了 2021/2/14
        result['Stable'] = line.select('td.Trainer')[0].span.text # 厩舎
        # エラー対応 2021/2/12：トレーナーIDのないデータに対応
        # ----Before----
        # result['TrainerId'] = re.search(r'\d+', line.select('td.Trainer')[0].a['href']).group()
        # ----After----
        if re.search(r'\d+', line.select('td.Trainer')[0].a['href']) is None:
            result['TrainerId'] = 'no_id'
        else:
            result['TrainerId'] = re.search(r'\d+', line.select('td.Trainer')[0].a['href']).group()
        # エラー対応終了 2021/2/12
        result['Trainer'] = line.select('td.Trainer')[0].a.text
        RaceResult.append(result)
    return [RaceInfo, RaceResult]
        
def extractHorseInfoFromHtml(html):
    soup = BeautifulSoup(html.text, 'html.parser', from_encoding='EUC-JP')
    horse_info = {}
    horse_info['id'] = re.search(r'\d+', soup.find('meta', attrs={'property': 'og:url', 'content': True})['content']).group()
    horse_info['name'] = soup.select('div.horse_title')[0].h1.text.strip()
    horse_info['is_active'] = 0 if soup.select('div.horse_title p.txt_01')[0].text.split('　')[0] == '抹消' else 1
    if soup.select('div.horse_title p.txt_01')[0].text.split('　')[1] == '' \
       and soup.select('div.horse_title p.txt_01')[0].text.split('　')[2].strip() == '':
        horse_info['sex'] = 'no_data'
        horse_info['color'] = 'no_data'
    else:
        horse_info['sex'] = soup.select('div.horse_title p.txt_01')[0].text.split('　')[1][0]
        horse_info['color'] = soup.select('div.horse_title p.txt_01')[0].text.split('　')[2].strip()
    horse_info['birthday'] = soup.select('.db_prof_table > tr')[0].td.text
    row4th = soup.select('.db_prof_table > tr')[3].th.text
    if '募集情報' in row4th:
        horse_info['farm'] = soup.select('.db_prof_table > tr')[4].td.text.replace('\'', '')
        horse_info['total_wins'] = soup.select('.db_prof_table > tr')[7].td.text.replace('\n', '').split('/')[0].replace('(中央)', '').strip()
    else:
        horse_info['farm'] = soup.select('.db_prof_table > tr')[3].td.text.replace('\'', '')
        horse_info['total_wins'] = soup.select('.db_prof_table > tr')[6].td.text.replace('\n', '').split('/')[0].replace('(中央)', '').strip()
    horse_info['blood_father'] = soup.select('.blood_table > tr > td')[0].text.replace('\n', '').replace('\'', '')
    horse_info['blood_mother'] = soup.select('.blood_table > tr > td')[3].text.replace('\n', '').replace('\'', '')
    horse_info['blood_father_father'] = soup.select('.blood_table > tr > td')[1].text.replace('\n', '').replace('\'', '')
    horse_info['blood_father_mother'] = soup.select('.blood_table > tr > td')[2].text.replace('\n', '').replace('\'', '')
    horse_info['blood_mother_father'] = soup.select('.blood_table > tr > td')[4].text.replace('\n', '').replace('\'', '')
    horse_info['blood_mother_mother'] = soup.select('.blood_table > tr > td')[5].text.replace('\n', '').replace('\'', '')
    return horse_info

def extractJockeyInfoFromHtml(html):
    soup = BeautifulSoup(html.text, 'html.parser', from_encoding='EUC-JP')
    jockey_info = {}
    infotable = soup.select('td.txt_l')
    jockey_info['id'] = re.search(r'\d+', soup.find('meta', attrs={'property': 'og:url', 'content': True})['content']).group()
    jockey_info['name'] = soup.select('div.db_head_name')[0].h1.text.replace('\n', '').replace('\xa0', '')
#     print(infotable)
    if len(infotable) == 8:
        jockey_info['home_town'] = infotable[0].text
        jockey_info['height'] = infotable[2].text
        jockey_info['weight'] = infotable[3].text
        jockey_info['blood_type'] = infotable[1].text
    return jockey_info

def extractRaceShutubaFromHtml(html):
    soup = BeautifulSoup(html.text, 'html.parser', from_encoding='EUC-JP')
    # レース結果がない場合終了する
    if soup.find(attrs={'class': 'RaceName'}) is None:
        raise NoResultError()
        
    RaceInfo = {} # レース情報
    RaceHorses = [] # レース結果
    # レース情報を取得する
    RaceInfo['RaceId'] = re.search(r'\d+', soup.find('meta', attrs={'property': 'og:url', 'content': True})['content']).group()
    ## レース日
    RaceInfo['Date'] = soup.select('#RaceList_DateList dd.Active')[0].text
    ## レース場
    RaceInfo['Place'] = soup.select('div.RaceData02 span')[1].text
    ## レース番号
    RaceInfo['RaceNum'] = soup.find_all(attrs={'class': 'RaceNum'})[0].text
    ## レース名
    RaceInfo['RaceName'] = soup.find_all(attrs={'class': 'RaceName'})[0].text.strip()
    ## レース時間/レース場/天候/馬場
    elems = soup.find_all(attrs={'class': 'RaceData01'})[0].text.strip().replace(' ', '').replace('\n', '').split('/')
    RaceInfo['StartTime'] = elems[0]
    RaceInfo['Field'] = elems[1]
    try:
        RaceInfo['Weather'] = elems[2].split(':')[1]
        RaceInfo['Baba'] = elems[3].split(':')[1]
    except Exception as e:
        print(e)
        RaceInfo['Weather'] = ''
        RaceInfo['Baba'] = ''
    ## 距離/地面/方向（Fieldより抽出）
    tmp = re.match(r'(.)(\d{4})m\((.+)\)', elems[1])
    RaceInfo['Surface'] = tmp.group(1)
    RaceInfo['Distance'] = tmp.group(2)
    RaceInfo['Direction'] = tmp.group(3)
    ## レース種別/グレード
    gradeIcon = ''
    raceNameObj = soup.select('.RaceName > span')
    if len(raceNameObj) > 0:
        for c in raceNameObj[0]['class']:
            if c == 'Icon_GradeType1':
                gradeIcon = 'G1'
            elif c == 'Icon_GradeType2':
                gradeIcon = 'G2'
            elif c == 'Icon_GradeType3':
                gradeIcon = 'G3'
            else:
                pass
    raceInfoObj = soup.select('.RaceData02 > span')
    if gradeIcon == '':
        RaceInfo['Grade'] = converConditionChar(raceInfoObj[3].text)+converGradeChar(raceInfoObj[4].text)
    else:
        RaceInfo['Grade'] = gradeIcon
        
    # レース結果を取得する
    HorseList = soup.select('tbody > .HorseList')
    for line in HorseList:
        if len(line.select('.Horse02')) == 0:
            break
        racehorse = {}
        cols = line.select('td')
        racehorse['waku'] = cols[0].text
        racehorse['umaban'] = cols[1].text
        racehorse['horse_name'] = cols[3].select('.Horse02')[0].a.text
        racehorse['horse_id'] = re.search(r'\d+', cols[3].select('.Horse02')[0].a['href']).group()
        racehorse['stable'] = cols[3].select('.Horse05')[0].a.text.split('・')[0]
        racehorse['trainer'] = cols[3].select('.Horse05')[0].a.text.split('・')[1]
        racehorse['trainer_id'] = re.search(r'\d+', cols[3].select('.Horse05')[0].a['href']).group()
        racehorse['sex'] = re.split(r'\d+', cols[4].select('.Barei')[0].text)[0]
        racehorse['color'] = re.split(r'\d+', cols[4].select('.Barei')[0].text)[1]
        racehorse['age'] = re.search(r'\d+', cols[4].select('.Barei')[0].text).group()
        racehorse['jockey'] = cols[4].a.text
        racehorse['jockey_id'] = re.search(r'\d+', cols[4].a['href']).group()
        try:
            racehorse['leg'] = converLeg(re.search(r'horse_race_type0\d', cols[3].select('.Horse06')[0].img['src']).group())
            # racehorse['weight'] = cols[3].select('.Horse07 > .Weight')[0].text.split('(')[0][:-2]
            # racehorse['weight_change'] = cols[3].select('.Horse07 > .Weight')[0].text.split('(')[1][:-1]
            # racehorse['jockey_weight'] = cols[4].select('span')[1].text
        except Exception as e:
            print(e)
            racehorse['leg'] = ''
            # racehorse['weight'] = np.nan
            # racehorse['weight_change'] = np.nan
            # racehorse['jockey_weight'] = np.nan
        RaceHorses.append(racehorse)
        
    return RaceInfo, RaceHorses


# %%
if __name__ == "__main__":
    print("競馬データ取得用のカスタムスクレイピングモジュールです")

