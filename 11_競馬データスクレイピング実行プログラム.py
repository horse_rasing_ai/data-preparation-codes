#!/usr/bin/env python
# coding: utf-8

# ## 分析用競馬データスクレイピング

# %%

import custom_modules.myscraping as mysc
import MySQLdb
import time
from tqdm import tqdm
import re
    
def fetch_raceid_by_year(year, condition, limit):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
 
    # カーソルを取得する
    cur= con.cursor()

    # クエリを実行する
    sql = "select id, status from `t_race_ids` where `id` like '{}%' and `status` = '{}' limit {}".format(year, condition, limit)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
 
    cur.close()
    con.close()
    
    return rows


def executeSQLs(sqls):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
    
    # カーソルを取得する
    cur= con.cursor()
    
    # sqlを実行する
    for sql in sqls:
        cur.execute(sql)
    con.commit()
    
    # 終了処理
    cur.close()
    con.close()

    
def isExistRaceInfo(raceId):
    # 接続する 
    con = MySQLdb.connect(
            user='user',
            passwd='secret',
            host='127.0.0.1',
            db='keiba_db',
            charset="utf8")
    
    # カーソルを取得する
    cur= con.cursor()

    # sqlを実行する
    sql = "select * from `t_race_info` where `id` = '{}'".format(raceId)
    cur.execute(sql)

    # 実行結果をすべて取得する
    rows = cur.fetchall()
    
    # 終了処理
    cur.close()
    con.close()

    if len(rows) == 0:
        return False
    else:
        return True
    
def scrapingkeibadata(year, condition, limit):
    record_labels = ['Rank', 'Waku', 'Umaban', 'HorseId', 'HorseName', 'Sex', 'Age', 'Weight', 'WeightChange', 'JockeyId', 'JockeyName', 'JockeyWeight', 'Time', 'Time3F', 'PassageRate','Ninki', 'Odds', 'Stable', 'TrainerId', 'Trainer']
    idlist = fetch_raceid_by_year(year, condition, limit)
    sqls = []
    count = 1
    for item in tqdm(idlist):
#         print("{} Proccess is start. RaceId: {}".format(count, item[0]))
        count += 1
        time.sleep(1)
        try:
            html = mysc.getRaceResultHtml(item[0])
            RaceInfo, RaceResult = mysc.extractRaceResutFromHtml(html)
            # レース情報書き込みsql作成
            if not isExistRaceInfo(item[0]):
                # 日付情報を含めたレースIDを作成
                match = re.match(r'(\d{1,2})月(\d{1,2})日\(.\)', RaceInfo['Date'])
                match2 = re.match(r'(\d{1,2})/(\d{1,2})', RaceInfo['Date'])
                date_formatted = ''
                if not(match is None) and len(match.groups()) == 2:
                    month = ("0" + match.groups()[0])[-2:]
                    day = ("0" + match.groups()[1])[-2:]
                    date_formatted = RaceInfo['RaceId'][:4] + month + day + RaceInfo['RaceId'][4:]
                if not(match2 is None) and len(match2.groups()) == 2:
                    month = ("0" + match2.groups()[0])[-2:]
                    day = ("0" + match2.groups()[1])[-2:]
                    date_formatted = RaceInfo['RaceId'][:4] + month + day + RaceInfo['RaceId'][4:]                  
                sql_write_race_info = "insert into t_race_info (`id`, `name`, `date`, `date_formatted`, `place_name`, `race_number`, `start_time`, `field`, `weather`, `baba`, `surface`, `distance`, `direction`, `grade`,"\
                                      "`odds_win`, `odds_show_1st`, `odds_show_2nd`, `odds_show_3rd`, `odds_bracket_quinella`, `odds_quinella`, `odds_quinella_place_1st_2nd`, `odds_quinella_place_1st_3rd`, `odds_quinella_place_2nd_3rd`, `odds_exacta`, `odds_trio`, `odds_trifecta`)"\
                                      " values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{24}')".format(
                                        RaceInfo['RaceId'], RaceInfo['RaceName'], RaceInfo['Date'], date_formatted, RaceInfo['Place'], RaceInfo['RaceNum'],
                                        RaceInfo['StartTime'], RaceInfo['Field'],RaceInfo['Weather'], RaceInfo['Baba'],
                                        RaceInfo['Surface'], RaceInfo['Distance'], RaceInfo['Direction'], RaceInfo['Grade'],
                                        RaceInfo['odds_win'], RaceInfo['odds_show_1st'], RaceInfo['odds_show_2nd'], RaceInfo['odds_show_3rd'], RaceInfo['odds_bracket_quinella'],
                                        RaceInfo['odds_quinella'], RaceInfo['odds_quinella_place_1st_2nd'], RaceInfo['odds_quinella_place_1st_3rd'], RaceInfo['odds_quinella_place_2nd_3rd'],
                                        RaceInfo['odds_exacta'], RaceInfo['odds_trio'], RaceInfo['odds_trifecta']
                                    )
                sqls.append(sql_write_race_info)
            # レース結果書き込みsql作成
            sql_write_race_record = "insert into t_race_records (`id`, `race_id`, `rank`, `wakuban`, `umaban`, `horse_id`, `horse_name`, `sex`, `age`,"\
                                    "`weight`, `weight_change`, `jockey_id`, `jockey_name`, `jockey_weight`,`time_all`, `time_3f`,"\
                                    "`passage_rate`, `ninki`, `odds`, `stable`, `trainer_id`, `trainer_name`) values "
            if len(RaceResult) == 0:
                raise mysc.NoResultError
            for index, record in enumerate(RaceResult):
                # 同着の場合があるため、レコードIDは「レースID+記録の順番」としている（Rankを使うとキー重複エラーとなる場合が出る）
                sql_write_race_record_value = "('{}', '{}'".format(RaceInfo['RaceId']+("0"+str(index))[-2:], RaceInfo['RaceId'])
                for label in record_labels:
                    sql_write_race_record_value += ", '{}'".format(record[label])
                sql_write_race_record_value += ")"
                sql_write_race_record += sql_write_race_record_value + ","
            sqls.append(sql_write_race_record[:-1]) # 末尾にカンマがついているため除去する
            update_status_raceid = "update t_race_ids set status = 'データ取得完了' where id = {}".format(item[0])
            sqls.append(update_status_raceid)
        except mysc.NoResultError:
            sql = "update t_race_ids set status = 'レース結果無し' where id = {}".format(item[0])
            sqls.append(sql)
            pass
        except Exception as e:
            print(item[0], e)
            # raise e
            sql = "update t_race_ids set status = 'データ取得エラー' where id = {}".format(item[0])
            sqls.append(sql)
        finally:
            # print(sqls)
            executeSQLs(sqls)
            sqls = []
#             print("{} Proccess is finished. RaceId: {}".format(count, item[0]))


if __name__ == "__main__":
    condition = '未処理'
    # condition = '処理エラー'
    print("処理を開始します")
    # last updated raceId: '2021040404','2021100404', '2021010204'
    races = [
             #202104030207(error), 
             #202102011103(error), 
             #202104040405(error),
            2021050503,2021090503,2021030203,
            2021050504,2021090504,2021030204,
            ]
    # Step.1
    for race in races:
        # 今回の方式では2007年7月28日以降のデータしか取得できない（登録なしの場合）
        scrapingkeibadata(race, condition, 1000)

    print("処理を終了します")

# %%
